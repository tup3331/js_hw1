let a = 1;
console.log(typeof a);

let name = 'Олександр';
let lastName = 'Бугера';
console.log("Мене звати", name, lastName);

// Або    console.log(`Мене звати ${name}, ${lastName}`);

let b = 10;
let mstr = `Word ${b} word`;
console.log(mstr);